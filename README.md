Program yang dapat memperbaiki kata tidak formal dalam Bahasa Indonesia menjadi kata formal

Program ini dibuat untuk memenuhi tugas akhir dari matakuliah Pembelajaran Mesin Program ini disusun oleh Kelompok 18:

    1. Melissa Panjaitan
    2. Herianto Saragi
    3. Glory Karina Hotbana Sitohang
    4. Jordy Hutahaean

Program ini terinspirasi dari:

1. Membuat Model Word2Vec Bahasa Indonesia dari Wikipedia Menggunakan Gensim, by Dieka Nugraha K https://medium.com/@diekanugraha/membuat-model-word2vec-bahasa-indonesia-dari-wikipedia-menggunakan-gensim-e5745b98714d
2. Build a spell-checker with word2vec data (with python), by Thomas Decaux https://medium.com/@thomasdecaux/build-a-spell-checker-with-word2vec-data-with-python-5438a9343afd
3. Creating a Spell Checker with TensorFlow, by David Currie https://towardsdatascience.com/creating-a-spell-checker-with-tensorflow-d35b23939f60
4. Normalisasi Mikroteks Berbentuk Kontekstual Berbahasa Indonesia pada Twitter Menggunakan Dictionary-Based dan Algoritma Longest Common Subsequence (LCS), http://repositori.usu.ac.id/handle/123456789/16147

Untuk Corpus yang digunakan dapat diunduh pada link berikut:

    1. https://dumps.wikimedia.org/idwiki/latest/ , Wiki Corpus in Indoensia
    2. https://raw.githubusercontent.com/nasalsabila/kamus-alay/master/colloquial-indonesian-lexicon.csv , Slang Word in Indonesia
    3. https://github.com/riochr17/Analisis-Sentimen-ID/blob/master/kamus/kbba.txt
    
